<?php

return [
    //调试模式
    'debug' => true,
    //开关
    'enable' => [
        //启用事件
        'event' => false,
        //启用队列
        'queue' => false,
    ],
    //队列名称
    'queue' => 'default',
];

