<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OkamiChen\ServiceApi\Support;

use Illuminate\Pipeline\Pipeline;
use Illuminate\Container\Container;
use OkamiChen\ServiceApi\Support\Container\Kernel;
use OkamiChen\ServiceApi\Support\Flow\PipeInterface;
use Exception;

/**
 * Description of HttpRequest
 *
 * @author www
 */
class HttpKernel {

    /**
     * 输出结果
     * @param \Illuminate\Http\Request $request
     * @param array $pipes
     * @param array $options
     * @return \Illuminate\Http\JsonResponse
     */
    static public function terminate($request, $pipes, $options=[]){
        
        $container  = new Container();
        $kernel     = new Kernel($request, $options);
        $pipe       = new Pipeline($container);
        
        try {
            $pipes      = array_map(function($class){
                if(!($class instanceof PipeInterface)){
                    $className  = get_class($class);
                    $pos    = strripos($className, '\\');
                    $str    = substr($className, $pos+1);
                    $message    = sprintf('[%s]必须实现[%s]接口', $str, 'PipeInterface');
                    throw new Exception($message);
                }
                return $class;
            }, $pipes);
        } catch (Exception $ex) {
            return $kernel->error(null, $ex->getMessage());
        }

        return $pipe->send($kernel)->through($pipes)->then(function(Kernel $kernel){
            return $kernel->send();
        });
    }
}
