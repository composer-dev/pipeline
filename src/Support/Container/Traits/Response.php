<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OkamiChen\ServiceApi\Support\Container\Traits;

use Carbon\Carbon;
/**
 * Description of Response
 *
 * @author www
 */
trait Response {

    /**
     * 成功
     * @param array|null $data
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function success($data = null, string $message = null) {
        $this->response = [
            'success' => true,
            'errorCode' => null,
            'errorMessage' => $message,
            'model' => $data,
        ];
        return $this->send();
    }

    /**
     * 验证数据不完整
     * @param array|null $data
     * @param string $message
     * @param string $errorCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function fail($data = null, string $message = null, $errorCode = 300) {
        $this->response = [
            'success' => false,
            'errorCode' => $errorCode,
            'errorMessage' => $message,
            'model' => $data,
        ];
        return $this->send();
    }

    /**
     * 服务器内部错误
     * @param array|null $data
     * @param string $message
     * @param string $errorCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function error($data = null, string $message = null, $errorCode = 500) {
        $this->response = [
            'success' => false,
            'errorCode' => $errorCode,
            'errorMessage' => $message,
            'model' => $data,
        ];
        return $this->send();
    }

    /**
     * 输出
     * @return \Illuminate\Http\JsonResponse
     */
    public function send() {

        $options    = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
        $addon      = $this->preResponse();
        
        if ($this->getOption('debug', false)) {

            $this->response['response'] = $addon;
        }
        
        return response()->json($this->response, 200, [], $options);
    }
    
    /**
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getResponse(){
        return $this->response;
    }
    
    protected function preResponse(){
        $end = new Carbon();
        $spend = sprintf('%0.6f', $end->format('s.u') - $this->start->format('s.u'));
        $this->addon['debug'] = [
            'time' => [
                'start' => $this->start,
                'end' => $end,
                'spend' => $spend,
            ],
            'request' => [
                'query' => $this->request->all(),
                'ip' => $this->request->getClientIps(),
                'agent' => $this->request->header('User-Agent'),
            ],
            'chain' => $this->chain,
        ];
        return $this->addon;
    }

}
