<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OkamiChen\ServiceApi\Support\Container\Traits;

/**
 * Description of Attribute
 *
 * @author www
 */
trait Attribute {

    /**
     *
     * @var \Carbon\Carbon 
     */
    public $start;

    /**
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     *
     * @var \Illuminate\Http\JsonResponse 
     */
    protected $response = null;

    /**
     *
     * @var \Illuminate\Support\Collection  
     */
    protected $options;

    /**
     *
     * @var \Illuminate\Support\Collection  
     */
    protected $chain;
    
    /**
     * 
     * @param string $key
     * @param object $value
     */
    public function option($key, $value) {
        $this->options->put($key, $value);
    }

    /**
     * 
     * @param string $key
     * @param object $default
     * @return object
     */
    public function getOption($key, $default = null) {
        return $this->options->get($key, $default);
    }

    /**
     * 
     * @return Request
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * 加入到调用链
     * @param string $chain
     */
    public function addChain($chain) {
        $this->chain->push($chain);
    }

    /**
     * 
     * @return \Illuminate\Support\Collection 
     */
    public function getChains() {
        return $this->chain;
    }

}
