<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OkamiChen\ServiceApi\Support\Container;

use Carbon\Carbon;
use Illuminate\Http\Request;
use OkamiChen\ServiceApi\Support\Container\Traits\Response;
use OkamiChen\ServiceApi\Support\Container\Traits\Attribute;

/**
 * Description of Kernel
 *
 * @author www
 */
class Kernel {

    use Attribute,Response;

    /**
     * 
     * @param Request $request
     */
    public function __construct(Request $request, $options=[]) {
        $this->options  = collect($options);
        $this->request  = $request;
        $this->chain    = collect();
        $this->start    = new Carbon();
    }
    
}
