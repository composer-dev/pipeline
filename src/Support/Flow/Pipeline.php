<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace OkamiChen\ServiceApi\Support\Flow;

use Closure;
use Exception;
use OkamiChen\ServiceApi\Support\Container\Kernel;
use OkamiChen\ServiceApi\Event\PipeProcessedEvent;
use OkamiChen\ServiceApi\job\PipeProcessedJob;

/**
 * Description of Test
 *
 * @author www
 */
class Pipeline {
    
    protected $errorCode;
    
    protected $errorMessage;
    
    protected $data;
    
    protected $logger;
    
    /**
     *
     * @var Kernel 
     */
    protected $kernel;


    public function handle(Kernel $kernel, Closure $next){
        
        $this->kernel   = $kernel;
        
        $this->debug();
            
        try {
            return $this->doRun($kernel, $next);
        } catch (Exception $ex) {
            $full       =   $this->logger($ex);
            $error      = array_only($full, ['error'])['error'];
            $this->data['logger']  = array_only($error,['id','server','name']);
            $message    = $this->errorMessage ?? $ex->getMessage();
            $errorCode  = $this->errorCode ?? 500;
            $response   = $kernel->error($this->data, $message, $errorCode);
            return $response;
        }
       

    }
            
    /**
     * 
     * @param type $name
     * @return type
     */
    protected function getLoggerChannel($name){
        $logger  = $this->kernel->getOption('logger', null);
        $channel = array_get($logger, $name, null);
        if(!$channel){
            return $channel;
        }
        if(!is_array($channel)){
            $channel    = [$channel];
        }
        return $channel;
    }
    
    /**
     * 记录异常信息
     * @param Exception $ex
     * @return array Description
     */
    protected function logger(Exception $ex) {
        
        $server     = $this->kernel->getRequest()->server('SERVER_ADDR');
        $server     = substr($server, strrpos($server, '.') + 1);
        $server     = sprintf('%03d', $server);
        $id         = uniqid() . strtolower(str_random(4));
        
        $message = [
            'error' => [
                'id'        => $id,
                'name'      => $this->getName(),
                'server'    => $server,
                'message'   => $ex->getMessage(),
                'file'      => $ex->getFile(),
                'line'      => $ex->getLine(),
            ],
            'request'   => $this->kernel->getRequest()->all(),
        ];
        
        $this->doLogger($id, $message);
        
        return $message;
    }
    
    protected function doLogger($id, $message){
        $channel    = $this->getLoggerChannel('error');
        
        if($channel){
            try {
                logger()->stack($channel)->error($id, $message);
                return true;
            } catch (Exception $ex) {
                return false;
            }
        }
        
    }

    

    protected function debug(){
        
        $debug  = $this->kernel->getOption('debug', false);
        
        if(!$debug){
            return true;
        }
        
        $this->dispatchToChain();

    }
    
    /**
     * 调用链
     */
    protected function dispatchToChain(){
        $class  = get_called_class();
        $count  = $this->kernel->getChains()->count();
        $time   = new \Carbon\Carbon();
        if(!$count){
            $spend  = sprintf('%0.6f', $time->format('s.u') - $this->kernel->start->format('s.u'));
        }else{
            $last = $this->kernel->getChains()->last();
            foreach ($last as $key => $value) {
                $spend  = sprintf('%0.6f', $time->format('s.u') - $value['time']->format('s.u'));
                break;
            }
        }

        $value = [
            'time'      => $time,
            'object'    => get_class_vars($class),
            'spend'     => $spend,
        ];
        
        $enable = $this->kernel->getOption('enable');
        
        if(array_get($enable, 'chain', false)){
            $this->kernel->addChain([$class=>$value]);
        }
        
        if(array_get($enable, 'event', false)){
            $value['request'] = $this->kernel->getRequest()->all();
            $this->dispatchToEvent($class, $value);
        }
        
        if(array_get($enable, 'queue', false)){
            $value['request'] = $this->kernel->getRequest()->all();
            $this->dispatchToJob($class, $value);
        }        
        
    }


    /**
     * 事件分配
     * @param type $class
     * @param type $value
     */
    protected function dispatchToEvent($class, $value) {
        
        try {
            $event = new PipeProcessedEvent($class, $value);
            event($event);
        } catch (Exception $ex) {
            $this->logger($ex);
        }
    }

    /**
     * 异步队列
     * @param type $class
     * @param type $value
     */
    protected function dispatchToJob($class, $value) {

        try {
            $job = new PipeProcessedJob($class, $value);
            $job->onQueue($this->kernel->getOption('queue', 'default'));
            $ret = dispatch($job);
        } catch (Exception $ex) {
            $this->logger($ex);
        }
    }

}
