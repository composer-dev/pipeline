<?php

namespace OkamiChen\ServiceApi\job;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PipeProcessedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $className;
    
    public $paramter;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($className, $paramter=[])
    {
        $this->className    = $className;
        $this->paramter     = $paramter;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

    }
}
