<?php

namespace OkamiChen\ServiceApi\Event;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PipeProcessedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $className;
    
    public $paramter;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($className, $paramter=[])
    {
        $this->className    = $className;
        $this->paramter     = $paramter;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
