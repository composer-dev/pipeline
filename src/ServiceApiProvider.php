<?php

namespace OkamiChen\ServiceApi;

use Illuminate\Support\ServiceProvider;

class ServiceApiProvider extends ServiceProvider {

    /**
     * @var array
     */
    protected $commands = [

    ];
    
    protected $observers    = [

    ];
    
    protected $listen = [
 
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        
        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__.'/../config' => config_path('configure')], 'service-api');
        }

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {

    }
    
}
