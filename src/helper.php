<?php

if(!function_exists('terminate')){
    
    /**
     * 
     * @param \Illuminate\Http\Request $request
     * @param array $options
     * @param array $pipes
     */
    function terminate($request, array $pipes, array $options=[]){
        return \OkamiChen\ServiceApi\Support\HttpKernel::terminate($request,$pipes,$options);
    }
}

